<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/search', 'ItemController@search')->name('search');
Route::get('/category/{category_id}/{category_name}', 'ItemController@search')->name('category');
Route::get('/item/{id}/{name}', 'ItemController@details')->name('item_details');

Route::post('/order-item', 'OrderController@addItemBasket')->name('order-item');
Route::put('/order-item/{id}', 'OrderItemController@put');
Route::delete('/order-item/{id}', 'OrderController@removeItemBasket');

Route::get('/basket', 'OrderController@basket')->name('basket');

Route::middleware('auth')->group(function () {
    Route::get('/address/zip-code', 'AddressController@zipCode');

    Route::get('/checkout/address-delivery', 'OrderController@addressDelivery')->name('checkout');
    Route::post('/checkout/address-delivery', 'OrderController@saveAddressDelivery');

    Route::get('/transaction/card-hash-key', 'TransactionController@cardHashKey');

    Route::get('/checkout/payment', 'OrderController@payment')->name('checkoutPayment');
    Route::post('/checkout/payment', 'OrderController@savePayment');

});