<?php
namespace App\Providers;

use App\Http\ViewComposers\BasketComposer;
use App\Http\ViewComposers\CategoryComposer;
use App\Http\ViewComposers\StoreComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // Using class based composers...
        View::composer('*', CategoryComposer::class);
        View::composer('*', BasketComposer::class);
        View::composer('*', StoreComposer::class);
    }
}
