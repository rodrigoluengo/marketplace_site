<?php
namespace App\Listeners;

use App\Services\OrderService;
use Illuminate\Events\Dispatcher;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;

class UserEventSubscriber
{
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Handle user login events.
     */
    public function onUserLogin(Login $event)
    {
        $order = $this->orderService->findBasket();
        if ($order['person'] === null) {
            $order['person_id'] = $event->user->person['id'];
            $this->orderService->save($order, $order['id']);
        }
    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout(Logout $event)
    {
        if ($event) {

        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }
}