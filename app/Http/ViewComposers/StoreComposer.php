<?php
namespace App\Http\ViewComposers;

use App\Services\StoreService;
use Illuminate\View\View;

class StoreComposer
{
    private $service;

    public function __construct(StoreService $service)
    {
        $this->service = $service;
    }

    public function compose(View $view)
    {
        $store = $this->service->find();
        $view->with('store', $store);
    }
}
