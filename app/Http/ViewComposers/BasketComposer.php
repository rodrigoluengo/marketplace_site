<?php
namespace App\Http\ViewComposers;

use App\Services\OrderService;
use Illuminate\View\View;

class BasketComposer
{
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function compose(View $view)
    {
        $basket = $this->orderService->findBasket();
        $view->with('basket', $basket);
    }
}
