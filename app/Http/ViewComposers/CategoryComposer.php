<?php
namespace App\Http\ViewComposers;

use App\Enums\HttpStatus;
use App\Services\CategoryService;
use Illuminate\View\View;

class CategoryComposer
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function compose(View $view)
    {
        $responseService = $this->categoryService->search();
        $categories = [];
        if ($responseService->status === HttpStatus::OK) {
            $categories = $responseService->data;
        }
        $view->with('categories', $categories);
    }
}
