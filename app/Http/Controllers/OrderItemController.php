<?php
namespace App\Http\Controllers;

use App\Services\OrderItemService;
use Illuminate\Http\Request;
use Cookie;

class OrderItemController extends Controller
{
    private $service;

    public function __construct(OrderItemService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $this->service->saveBasket($request->all());
        return redirect()->route('basket');
    }

    public function put(Request $request, $id)
    {
        $this->service->saveBasket($request->all(), $id);
        return redirect()->route('basket');
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return redirect()->route('basket');
    }
}
