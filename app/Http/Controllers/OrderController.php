<?php
namespace App\Http\Controllers;

use App\Http\Requests\AddressRequest;
use App\Http\Requests\PaymentRequest;
use App\Services\OrderService;
use Auth;
use Cookie;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    public function basket()
    {
        return view('basket');
    }

    public function addItemBasket(Request $request)
    {
        $this->service->addItemBasket($request->all());
        return redirect()->route('basket');
    }

    public function removeItemBasket($id)
    {
        $this->service->removeItemBasket($id);
        return redirect()->route('basket');
    }

    public function addressDelivery()
    {
        $addresses = Auth::user()->person['addresses'];
        $addressSelected = null;
        if ($addressSelected === null && empty($addresses) === false) {
            $addressSelected = collect($addresses)->filter(function ($address) {
                 return $address['pivot']['default'];
            })->first();
        }

        return view('checkout.address-delivery', [
            'addresses' => $addresses,
            'addressSelected' => $addressSelected
        ]);
    }

    public function saveAddressDelivery(Request $request)
    {
        $this->service->saveAddressDelivery($request->all());
        return redirect()->route('checkoutPayment');
    }

    public function payment()
    {
        return view('checkout.payment');
    }

    public function savePayment(PaymentRequest $request)
    {
        return $this->service->savePayment($request->all());
    }
}
