<?php
namespace App\Http\Controllers;

use App\Services\AddressService;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    private $service;

    public function __construct(AddressService $service)
    {
        $this->service = $service;
    }

    public function zipCode(Request $request)
    {
        return $this->service->zipCode($request->all());
    }
}