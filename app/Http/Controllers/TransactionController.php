<?php
namespace App\Http\Controllers;

use App\Services\TransactionService;

class TransactionController
{
    private $service;

    public function __construct(TransactionService $service)
    {
        $this->service = $service;
    }

    public function cardHashKey()
    {
        return $this->service->cardHashKey();
    }
}
