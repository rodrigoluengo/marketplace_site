<?php
namespace App\Http\Controllers;

use App\Services\ItemService;
use Illuminate\Http\Request;

class ItemController
{
    public $itemService;

    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    public function search(Request $request, $category_id = null)
    {
        $data = $request->all();
        if ($category_id !== null) {
            $data['category_id'] = $category_id;
        }
        $items = $this->itemService->search($data);
        return view('search', ['items' => $items->data]);
    }

    public function details($id)
    {
        $item = $this->itemService->find($id);
        return view('details', ['item' => $item->data]);
    }
}
