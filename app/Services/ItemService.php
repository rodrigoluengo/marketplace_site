<?php
namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ItemService extends HttpService
{
    public function search(array $data = [])
    {
        $data['status'] = true;
        return $this->get('item', $data);
    }

    public function find($id)
    {
        return $this->get("item/$id");
    }
}
