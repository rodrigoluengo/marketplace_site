<?php
namespace App\Services;

class TransactionService extends HttpService
{
    public function cardHashKey()
    {
        return $this->getAuthenticated('transaction/card-hash-key')->data;
    }
}
