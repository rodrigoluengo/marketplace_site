<?php
namespace App\Services;

class PersonService extends HttpService
{
    public function find($id)
    {
        return $this->getAuthenticated("person/$id")->data;
    }

    public function save(array $data, $id = null)
    {
        if ($id === null) {
            return $this->postAuthenticated("person", $data);
        }

        return $this->putAuthenticated("person/$id", $data);
    }

    public function remove($id)
    {
        return $this->deleteAuthenticated("person/$id");
    }
}
