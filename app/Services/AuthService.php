<?php
namespace App\Services;

use App\Enums\HttpStatus;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class AuthService extends HttpService
{
    public function login(array $data): ResponseService
    {
        return $this->post('auth', $data);
    }

    public function user()
    {
        $response = $this->getAuthenticated("auth");
        if ($response->status === HttpStatus::UNAUTHORIZED) {
            session()->flush();
        }
        return $response;
    }
}
