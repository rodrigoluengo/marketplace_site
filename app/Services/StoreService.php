<?php
namespace App\Services;

use Illuminate\Http\Request;

class StoreService extends HttpService
{
    public function find($id = null)
    {
        if ($id == null) {
            $request = app(Request::class);
            $id = $request->headers->get('host');
        }
        return $this->get("store/$id")->data;
    }
}