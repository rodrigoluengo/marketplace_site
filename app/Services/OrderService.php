<?php
namespace App\Services;

use App\Enums\HttpStatus;
use Cookie;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class OrderService extends HttpService
{
    private $personService;

    public function __construct(PersonService $personService)
    {
        $this->personService = $personService;
    }

    public function find($id)
    {
        return $this->get("order/$id")->data;
    }

    public function save(array $data, $id = null)
    {
        if ($id === null) {
            return $this->post("order", $data);
        }

        return $this->put("order/$id", $data);
    }

    public function remove($id)
    {
        return $this->delete("order/$id");
    }

    public function addItemBasket(array $data)
    {
        $order = $this->findBasket();
        if ($order === null) {
            $data = [
                'order_items' => [[
                    'item_id' => $data['item_id'],
                    'quantity' => $data['quantity']
                ]]
            ];
            $response = $this->save($data);
            $order = $response->data;
        } else {
            $exists = false;
            foreach ($order['order_items'] as &$order_item) {
                if ($order_item['item_id'] == $data['item_id']) {
                    $order_item['quantity'] += $data['quantity'];
                    $exists = true;
                    break;
                }
            }
            if ($exists === false) {
                $order['order_items'][] = ['item_id' => $data['item_id'], 'quantity' => $data['quantity']];
            }
            $response = $this->save($order, $order['id']);
        }
        Cookie::queue('order_id', $order['id'], WEEK_IN_MINUTES);
    }

    public function removeItemBasket($id)
    {
        $order = $this->findBasket();
        if ($order === null) {
            throw new ModelNotFoundException('order_not_found');
        }
        $order['order_items'] = collect($order['order_items'])->filter(function ($order_item) use ($id) {
            return $order_item['id'] != $id;
        })->toArray();
        if (empty($order['order_items'])) {
            return $this->remove($order['id']);
        }
        return $this->put("order/{$order['id']}", $order)->data;
    }

    public function findBasket()
    {
        $order_id = Cookie::get('order_id');
        $response = $this->get("order/$order_id");
        if ($response->status !== HttpStatus::OK) {
            Cookie::forget('order_id');
            return null;
        }
        return $response->data;
    }

    public function saveAddressDelivery(array $data)
    {
        $order_id   = array_get($data, 'order_id');
        $address_id = array_get($data, 'address_id');
        $address    = array_get($data, 'address');
        $order      = $order_id === null ? $this->findBasket() : $this->find($order_id);
        if ($order === null) {
            throw new ModelNotFoundException('order_not_found');
        }
        if ($address_id === null) {
            $person = $order['person'];
            if (empty($person['addresses'])) {
                $address['default'] = true;
                $address['name'] = 'Principal';
            }
            $person['addresses'][] = $address;
            $this->personService->save($person, $person['id']);
            $person = $this->personService->find($person['id']);
            $address_inserted = collect($person['addresses'])->last();
            $address_id = $address_inserted['id'];
        }
        $order['address_id'] = $address_id;
        $this->save($order, $order['id']);
    }

    public function savePayment(array $data)
    {
        $order_id = array_get($data, 'order_id');
        $order = $order_id === null ? $this->findBasket() : $this->find($order_id);
        if ($order === null) {
            throw new ModelNotFoundException('order_not_found');
        }

        return $this->postAuthenticated("order/{$order['id']}/payment", $data)->data;
    }
}
