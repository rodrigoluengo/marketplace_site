<?php
namespace App\Services;

use App\Enums\HttpStatus;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;

class ResponseService
{
    public $response;
    public $data;
    public $status;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
        $this->data = json_decode($response->getBody()->getContents(), true);
        $this->status = $response->getStatusCode();
        $this->putAuthorization();

        switch ($this->status) {
            case HttpStatus::UNPROCESSABLE_ENTITY:
                throw ValidationException::withMessages($this->data);
                break;
            case HttpStatus::UNAUTHORIZED:
                $authenticated = session('authenticated');
                if ($authenticated !== null) {
                    session()->forget('authenticated');
                    throw new AuthenticationException();
                }
                break;
        }
    }

    private function putAuthorization()
    {
        $authenticated = session('authenticated');
        $authorization = $this->response->getHeaderLine('Authorization');

        if ($authenticated !== null && !empty($authorization)) {
            $authenticated['token_type']    = strtolower(explode(' ', $authorization)[0]);
            $authenticated['access_token']  = explode(' ', $authorization)[1];
            session(['authenticated' => $authenticated]);
            return;
        }

        if (is_array($this->data) && array_key_exists('token_type', $this->data) && array_key_exists('access_token', $this->data)) {
            session(['authenticated' => $this->data]);
        }
    }

    public function __toString()
    {
        return json_encode((array) $this);
    }

    public function toArray()
    {
        return (array) $this;
    }
}
