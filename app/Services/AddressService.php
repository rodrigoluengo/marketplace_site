<?php
namespace App\Services;

class AddressService extends HttpService
{
    public function zipCode(array $data)
    {
        return $this->getAuthenticated("address/zip-code", $data)->data;
    }
}
