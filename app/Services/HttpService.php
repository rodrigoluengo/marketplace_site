<?php
namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class HttpService
{
    public function __call($name, $arguments): ResponseService
    {
        $name   = explode('_', snake_case($name));
        $method = strtoupper($name[0]);
        $path   = $arguments[0];
        $data   = [];
        if(count($arguments) > 1) {
            if ($method === 'GET' || $method === 'DELETE') {
                $data['query'] = $arguments[1];
            } else {
                $data['json'] = $arguments[1];
            }
        }
        $path   = env('APP_API') . '/' . $path;

        $authenticated = session('authenticated');
        $request = app(Request::class);
        $data['headers'] = [
            'Store' => $request->headers->get('host')
        ];

        if ($authenticated !== null && count($name) > 1) {
            $data['headers']['Authorization'] = "{$authenticated['token_type']} {$authenticated['access_token']}";
        }

        try {
            $client = new Client();
            $response = $client->request($method, $path, $data);
            $responseService = new ResponseService($response);
            return $responseService;
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $responseService = new ResponseService($response);
            return $responseService;
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $responseService = new ResponseService($response);
            return $responseService;
        }
    }
}
