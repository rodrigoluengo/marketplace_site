<?php
namespace App\Services;

class UserService extends HttpService
{
    public function insert(array $data): ResponseService
    {
        return $this->post('user', $data);
    }
}
