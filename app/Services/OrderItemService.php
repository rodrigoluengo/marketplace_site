<?php
namespace App\Services;

use Cookie;

class OrderItemService extends HttpService
{
    public function save(array $data, $id = null)
    {
        if ($id === null) {
            return $this->post('order-item', $data)->data;
        }
        return $this->put("order-item/$id", $data)->data;
    }

    public function saveBasket(array $data, $id = null)
    {
        $data['order_id'] = Cookie::get('order_id');
        $response = $this->save($data, $id);
        Cookie::queue('order_id', $response['order_id'], WEEK_IN_MINUTES);
    }

    public function delete($id)
    {
        return parent::delete("order-item/$id")->data;
    }
}
