<?php
namespace App\Services;

class CategoryService extends HttpService
{
    public function search (array $data = [])
    {
        return $this->getAuthenticated('category', $data);
    }
}
