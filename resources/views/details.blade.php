@extends('layouts.app')
@section('content')

    <section>

        <article>
            <header>
                <h2>{{ $item['name'] }}</h2>
            </header>
            <div class="row">

                <div class="col-xs-12 col-sm-6">

                    <div id="item-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach ($item['images'] as $i => $image)
                                <li data-target="#item-carousel" data-slide-to="{{ $i }}" @if ($i === 0) class="active" @endif></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach ($item['images'] as $i => $image)
                                <div class="carousel-item @if ($i === 0) active @endif">
                                    <img class="d-block w-100" src="{{ url('/storage/' . $image['filename']) }}">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#item-carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="carousel-control-next" href="#item-carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Próximo</span>
                        </a>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-6">
                    <p>{{ $item['price'] }}</p>
                    <form action="{{ route('order-item') }}" method="post">
                        @csrf
                        <input type="hidden" name="item_id" value="{{ $item['id'] }}">
                        <div class="form-control">
                            <label class="control-label">Quantidade</label>
                            <div class="input-group">
                                <input type="number" name="quantity" class="form-control" value="{{ old('quantity', 1) }}">
                                <div class="input-group-append">
                                    <button
                                        title="Adicionar ao carrinho de compras"
                                        class="btn btn-outline-secondary">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

            <h3>Detalhes</h3>
            {{ $item['details'] }}
        </article>

    </section>

@endsection

@push('scripts')
    <script type="text/javascript">


        $('.carousel').carousel()
    </script>
@endpush