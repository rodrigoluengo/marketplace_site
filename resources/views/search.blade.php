@extends('layouts.app')
@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Início</a></li>
        @if (request()->route()->getName() === 'category')
            <li class="breadcrumb-item"><a href="#">Category</a></li>
            <li class="breadcrumb-item active">Data</li>
        @else
        @endif
    </ol>

    @foreach($items['data'] as $i => $item)
        @if (($i % 4) === 0) <div class="row"> @endif
            <div class="col-xs-12 col-sm-4 col-md-3">
                <a class="item-anchor" href="{{ route('item_details', [$item['id'], str_slug($item['name'])]) }}">
                    <div class="item-image">
                        <img src="..." alt="..." class="img-rounded">
                    </div>
                    <div class="item-name">{{ $item['name'] }}</div>
                    <div class="item-price">{{ $item['price'] }}</div>
                </a>
            </div>
        @if (($i > 0 && ($i % 4) === 0) || count($items) === 1) </div> @endif
    @endforeach

@endsection
