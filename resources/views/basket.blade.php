@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    Carrinho de compras
                </div>
                @if ($basket === null)
                @else
                    <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Quantidade</th>
                            <th>Valor unit.</th>
                            <th>Desconto</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($basket['order_items'] as $order_item)
                            <tr>
                                <td>{{ $order_item['item']['name'] }}</td>
                                <td>{{ $order_item['quantity'] }}</td>
                                <td>{{ $order_item['price'] }}</td>
                                <td>{{ $order_item['discount'] ?? '-' }}</td>
                                <td>{{ $order_item['discount_total'] ?? $order_item['value'] }}</td>
                                <td>
                                    <form action="{{ url('order-item', $order_item['id']) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td>{{ $basket['quantity'] }}</td>
                            <td></td>
                            <td>{{ $basket['discount'] ?? '-' }}</td>
                            <td>{{ $basket['discount_total'] ?? $basket['value'] }}</td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                @endif
            </div>
            <a href="{{ url('/') }}">Continuar comprando</a>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <a href="{{ route('checkout') }}" class="btn">Fechar pedido</a>
            </div>
        </div>
    </div>
@endsection