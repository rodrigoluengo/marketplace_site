<div id="container_{{ $id }}_address">
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="{{ $id }}_zipcode">CEP</label>
            <div class="input-group">
                <input
                    class="form-control"
                    id="{{ $id }}_zipcode"
                    name="{{ $name }}[zipcode]"
                    value="{{ old($field . '.zipcode', array_get($address??[], 'zipcode')) }}"
                    data-category="zipcode">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="{{ $id }}_btn_zipcode">Buscar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="{{ $id }}_street">Endereço</label>
            <input
                class="form-control"
                id="{{ $id }}_street"
                name="{{ $name }}[street]"
                value="{{ old($field . '.street', array_get($address??[], 'street')) }}"
                data-category="street">
            <div class="invalid-feedback">
                Please choose a username.
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="{{ $id }}_number">Número</label>
            <input
                class="form-control"
                id="{{ $id }}_street_number"
                name="{{ $name }}[street_number]"
                value="{{ old($field . '.street_number', array_get($address??[], 'street_number')) }}">
        </div>
    </div>
    <div class="form-group">
        <label for="{{ $id }}_complement">Complemento</label>
        <input
            class="form-control"
            id="{{ $id }}_complement"
            name="{{ $name }}[complement]"
            value="{{ old($field . '.complement', array_get($address??[], 'complement')) }}">
    </div>
    <div class="form-row">
        <div class="form-group col-md-5">
            <label for="{{ $id }}_neighborhood">Bairro</label>
            <input
                class="form-control"
                id="{{ $id }}_neighborhood"
                name="{{ $name }}[neighborhood]"
                value="{{ old($field . '.neighborhood', array_get($address??[], 'neighborhood')) }}">
        </div>
        <div class="form-group col-md-4">
            <label for="{{ $id }}_city">Cidade</label>
            <input
                class="form-control"
                id="{{ $id }}_city"
                name="{{ $name }}[city]"
                value="{{ old($field . '.city', array_get($address??[], 'city')) }}">
        </div>
        <div class="form-group col-md-3">
            <label for="{{ $id }}_state">Estado</label>
            <select class="form-control" id="{{ $id }}_state" name="{{ $name }}[state]">
                <option value="">Selecione...</option>
                <option value="AC" @if(old($field . '.state', array_get($address??[], 'state')) === 'AC') selected @endif>Acre</option>
                <option value="AL" @if(old($field . '.state', array_get($address??[], 'state')) === 'AL') selected @endif>Alagoas</option>
                <option value="AP" @if(old($field . '.state', array_get($address??[], 'state')) === 'AP') selected @endif>Amapá</option>
                <option value="AM" @if(old($field . '.state', array_get($address??[], 'state')) === 'AM') selected @endif>Amazonas</option>
                <option value="BA" @if(old($field . '.state', array_get($address??[], 'state')) === 'BA') selected @endif>Bahia</option>
                <option value="CE" @if(old($field . '.state', array_get($address??[], 'state')) === 'CE') selected @endif>Ceará</option>
                <option value="DF" @if(old($field . '.state', array_get($address??[], 'state')) === 'DF') selected @endif>Distrito Federal</option>
                <option value="ES" @if(old($field . '.state', array_get($address??[], 'state')) === 'ES') selected @endif>Espírito Santo</option>
                <option value="GO" @if(old($field . '.state', array_get($address??[], 'state')) === 'GO') selected @endif>Goiás</option>
                <option value="MA" @if(old($field . '.state', array_get($address??[], 'state')) === 'MA') selected @endif>Maranhão</option>
                <option value="MT" @if(old($field . '.state', array_get($address??[], 'state')) === 'MT') selected @endif>Mato Grosso</option>
                <option value="MS" @if(old($field . '.state', array_get($address??[], 'state')) === 'MS') selected @endif>Mato Grosso do Sul</option>
                <option value="MG" @if(old($field . '.state', array_get($address??[], 'state')) === 'MG') selected @endif>Minas Gerais</option>
                <option value="PA" @if(old($field . '.state', array_get($address??[], 'state')) === 'PA') selected @endif>Pará</option>
                <option value="PB" @if(old($field . '.state', array_get($address??[], 'state')) === 'PB') selected @endif>Paraíba</option>
                <option value="PR" @if(old($field . '.state', array_get($address??[], 'state')) === 'PR') selected @endif>Paraná</option>
                <option value="PE" @if(old($field . '.state', array_get($address??[], 'state')) === 'PE') selected @endif>Pernambuco</option>
                <option value="PI" @if(old($field . '.state', array_get($address??[], 'state')) === 'PI') selected @endif>Piauí</option>
                <option value="RJ" @if(old($field . '.state', array_get($address??[], 'state')) === 'RJ') selected @endif>Rio de Janeiro</option>
                <option value="RN" @if(old($field . '.state', array_get($address??[], 'state')) === 'RN') selected @endif>Rio Grande do Norte</option>
                <option value="RS" @if(old($field . '.state', array_get($address??[], 'state')) === 'RS') selected @endif>Rio Grande do Sul</option>
                <option value="RO" @if(old($field . '.state', array_get($address??[], 'state')) === 'RO') selected @endif>Rondônia</option>
                <option value="RR" @if(old($field . '.state', array_get($address??[], 'state')) === 'RR') selected @endif>Roraima</option>
                <option value="SC" @if(old($field . '.state', array_get($address??[], 'state')) === 'SC') selected @endif>Santa Catarina</option>
                <option value="SP" @if(old($field . '.state', array_get($address??[], 'state')) === 'SP') selected @endif>São Paulo</option>
                <option value="SE" @if(old($field . '.state', array_get($address??[], 'state')) === 'SE') selected @endif>Sergipe</option>
                <option value="TO" @if(old($field . '.state', array_get($address??[], 'state')) === 'TO') selected @endif>Tocantins</option>
            </select>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#{{ $id }}_zipcode').setMask('99999-999');
        $('#{{ $id }}_btn_zipcode').click(function () {
            $('#container_{{ $id }}_address').removeClass('was-invalidated');
            $.getJSON('{{ url('/address/zip-code') }}', {
                'zipcode': $('#{{ $id }}_zipcode').val()
            })
            .done(function (response) {
                $('#container_{{ $id }}_address').find('.is-invalid').removeClass('is-invalid');
                $('#container_{{ $id }}_address').find('.text-danger').remove();

                $('#{{ $id }}_street').val(response.street);
                $('#{{ $id }}_neighborhood').val(response.neighborhood);
                $('#{{ $id }}_city').val(response.city);
                $('#{{ $id }}_state').val(response.state);

                var $emptyInput = $('#{{ $id }}_street, #{{ $id }}_number, #{{ $id }}_neighborhood, #{{ $id }}_city').filter(function () {
                    return $.trim($(this).val()) === '';
                });
                if ($emptyInput.length) {
                    $emptyInput.first().focus();
                }
            })
            .fail(function (response) {
                if (response.status === 422) {
                    $('#container_{{ $id }}_address').addClass('was-invalidated');
                    var errors = response.responseJSON.errors;
                    for (var error in errors) {
                        $('#container_{{ $id }}_address')
                            .find('[data-category=' + error + ']')
                            .addClass('is-invalid')
                            .closest('.form-group')
                            .each(function () {
                                var $formGroup = $(this);
                                $formGroup.find('.text-danger').remove();
                                $.each(errors[error], function () {
                                    const invalidFeedback = '<div class="text-danger">' + this + '</div>';
                                    $formGroup.append(invalidFeedback);
                                })
                            });
                    }
                    $('#container_{{ $id }}_address').find('[data-category]').first().select()
                }
            });
        });
    </script>
@endpush