@extends('layouts.app')
@section('content')
    <form action="{{ url('checkout/address-delivery') }}" method="post">
        @csrf
        <input
            type="hidden"
            id="address_id"
            name="address_id"
            value="{{ $basket['address_id'] ?? $addressSelected['id'] }}">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item"><a class="nav-link active">Endereço de entrega</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url('checkout/payment') }}">Pagamento</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url('checkout/confirmation') }}">Confirmação</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="row">
                            <div class="col-12 col-sm-3">
                                <div id="listGroupAddress" class="list-group list-group-flush" role="tablist">
                                    @foreach ($addresses as $address)
                                        <a
                                            class="list-group-item list-group-item-action{{ ($basket['address_id'] ?? $addressSelected['id']) === $address['id']  ? ' active' : ''}}"
                                            id="address-{{ $address['id'] }}-list"
                                            data-toggle="list"
                                            data-address-id="{{ $address['id'] }}"
                                            href="#address-{{ $address['id'] }}"
                                            role="tab">
                                            {{ $address['pivot']['name'] }}
                                        </a>
                                    @endforeach
                                    <a
                                        class="
                                            list-group-item
                                            list-group-item-action
                                            {{ empty(Auth::user()->person['addresses']) ? 'active' : '' }}
                                                "
                                        id="address-new-list"
                                        data-toggle="list"
                                        data-address-id=""
                                        href="#address-new"
                                        role="tab">
                                        Novo endereço de entrega
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-9">
                                <div class="tab-content">
                                    @foreach (Auth::user()->person['addresses'] as $i => $address)
                                        <div
                                            class="tab-pane active"
                                            id="address-{{ $address['id'] }}"
                                            role="tabpanel"
                                            aria-labelledby="address-{{ $address['id'] }}-list">
                                        </div>
                                    @endforeach
                                    <div
                                        class="
                                            tab-pane
                                            {{ empty(Auth::user()->person['addresses']) ? 'active' : '' }}
                                                "
                                        id="address-new"
                                        role="tabpanel"
                                        aria-labelledby="address-new-list">
                                        @include('address', [
                                            'id' => 'delivery',
                                            'name' => 'address',
                                            'field' => 'address'
                                        ])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button class="btn btn-primary">Próximo</button>
            </div>
        </div>
    </form>
@endsection
@push('scripts')
    <script type="text/javascript">
        $('#listGroupAddress a').on('click', function () {
            $('#address_id').val($(this).data('addressId'));
        })
    </script>
@endpush