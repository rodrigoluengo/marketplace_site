@extends('layouts.app')
@section('content')
    <form id="frmCheckoutPayment">
        @csrf
        <input type="hidden" name="payment_method" value="{{ old('payment_method', 'credit_card') }}">
        <input type="hidden" name="card_hash" value="{{ old('card_hash') }}">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item"><a class="nav-link" href="{{ url('checkout/address-delivery') }}">Endereço de entrega</a></li>
                    <li class="nav-item"><a class="nav-link active">Pagamento</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url('checkout/confirmation') }}">Confirmação</a></li>
                </ul>
                <div class="tab-content">
                    <div class="col-12 col-sm-3">
                        <div id="listGroupPayment" class="list-group list-group-flush" role="tablist">
                            <a
                                class="
                                    list-group-item
                                    list-group-item-action
                                    active
                                    "
                                id="tab-payment-credit-card"
                                data-toggle="list"
                                data-payment-method="credit_card"
                                href="#payment-credit-card"
                                role="tab">
                                Cartão de Crédito
                            </a>
                            <a
                                    class="
                                        list-group-item
                                        list-group-item-action
                                        "
                                    id="tab-payment-boleto"
                                    data-toggle="list"
                                    data-payment-method="boleto"
                                    href="#payment-boleto"
                                    role="tab">
                                Boleto
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-9">
                        <div class="tab-content">
                            <div
                                class="tab-pane active"
                                id="payment-credit-card"
                                role="tabpanel"
                                aria-labelledby="tab-payment-credit-card">

                                <div class="form-group">
                                    <label>Número do cartão</label>
                                    <input class="form-control" id="card_number" value="5532976619485623">
                                </div>

                                <div class="form-group">
                                    <label>Nome impresso no cartão</label>
                                    <input class="form-control" id="card_holder_name" name="card_holder_name" value="Rodrigo Pereira Luengo">
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm-4 form-group">
                                        <label>Validade</label>
                                        <input class="form-control" id="card_expiration_date" value="01/19">
                                    </div>
                                    <div class="col-12 col-sm-3 form-group">
                                        <label>CVV</label>
                                        <input class="form-control" id="card_cvv" value="712">
                                    </div>
                                </div>

                                <div class="form-group form-check">
                                    <input
                                        type="checkbox"
                                        class="form-check-input"
                                        id="billingAddressEqualDeliveryAddress"
                                        checked>
                                    <label class="form-check-label" for="billingAddressEqualDeliveryAddress">
                                        Endereço de cobrança é o mesmo endereço de entrega?
                                    </label>
                                </div>

                                @include('address', [
                                    'id' => 'billing',
                                    'name' => 'billing[address]',
                                    'field' => 'billing.address',
                                    'address' => $basket['address']
                                ])

                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="1" id="saveCreditCard" checked>
                                        <label class="form-check-label" for="saveCreditCard">
                                            Salvar cartão de crédito
                                        </label>
                                    </div>
                                </div>

                            </div>
                            <div
                                class="tab-pane"
                                id="payment-boleto"
                                role="tabpanel"
                                aria-labelledby="tab-payment-boleto">
                                [ BOLETO ]
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button class="btn btn-primary">Próximo</button>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script>
        $('#listGroupPayment > a').click(function () {
            $('input[name=payment_method]').val($(this).data('paymentMethod'))
        });

        $('#frmCheckoutPayment').submit(function (e) {
            if ($('input[name=payment_method]').val() == 'credit_card') {
                e.preventDefault();

                $('.is-invalid').removeClass('is-invalid');
                $('.invalid-feedback').remove();

                var card = {};
                card.card_number = $("#card_number").val();
                card.card_holder_name = $("#card_holder_name").val();
                card.card_expiration_date = $("#card_expiration_date").val().replace(/[^0-9]/g, '');
                card.card_cvv = $("#card_cvv").val();

                // pega os erros de validação nos campos do form e a bandeira do cartão
                var cardValidations = pagarme.validate({card: card});

                var errors = [];
                if (cardValidations.card.card_number === false) {
                    errors.push({'card_number': ['Informe um número de cartão de crédito válido']});
                }

                if (cardValidations.card.card_holder_name === false) {
                    errors.push({'card_holder_name': ['Informe o nome como está impresso no cartão']});
                }

                if (cardValidations.card.card_expiration_date === false) {
                    errors.push({'card_expiration_date': ['Informe a validade corretamente']});
                }

                if (cardValidations.card.card_cvv === false) {
                    errors.push({'card_cvv': ['Informe o CVV corretamente']});
                }

                if (errors.length) {
                    for (var i = 0; i < errors.length; i++) {
                        for (var error in errors[i]) {
                            $('#' + error).addClass('is-invalid');
                            var messages = errors[i][error];
                            var htmlInvalidFeedback = '<div class="invalid-feedback">';
                            for (var message in messages) {
                                htmlInvalidFeedback += messages[message] + '<br>'
                            }
                            htmlInvalidFeedback += '<div>';
                            $('#' + error).after(htmlInvalidFeedback);
                        }
                    }
                    $('.is-invalid:first').focus();
                } else {

                    $.getJSON('{{ url('/transaction/card-hash-key') }}', null, function (response) {
                        //card.card_expiration_date = card.card_expiration_date.replace(/[^0-9]/g, '');
                        var publicKey = forge.pki.publicKeyFromPem(response.public_key);
                        var buffer = forge.util.createBuffer($.param(card), 'utf8');
                        var bytes = buffer.getBytes();
                        var encrypted = publicKey.encrypt(bytes, 'RSAES-PKCS1-V1_5');
                        var card_hash = response.id + '_' + forge.util.encode64(encrypted);
                        $('input[name=card_hash]').val(card_hash);

                        $.post('{{ url('checkout/payment') }}', $('#frmCheckoutPayment').serialize(), function () {
                            //window.location.href = '{{ url('checkout/confirmation') }}'
                        })
                    })
                }
            }
        })
    </script>
@endpush