@extends('layouts.app')
@section('content')


            {{--<div class="card">
                <div class="card-body">
                    <form action="{{ route('checkout') }}" method="post">
                        {{ csrf_field() }}
                        @guest
                            <h2>Cadastro</h2>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="user_email">E-mail</label>
                                    <input
                                        type="email"
                                        class="form-control"
                                        id="user_email"
                                        name="user[email]"
                                        value="{{ old('user.email') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phone_number">Telefone</label>
                                    <input
                                        class="form-control"
                                        id="phone_number"
                                        name="person[phones][0][number]"
                                        value="{{ old('person.phones.0.number') }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="user_password">Senha</label>
                                    <input
                                        type="password"
                                        class="form-control"
                                        id="user_password"
                                        name="user[password]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="user_password_confirmation">Confirme a senha</label>
                                    <input
                                        type="password"
                                        class="form-control"
                                        id="user_password_confirmation"
                                        name="user[password_confirmation]">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="person_name">Nome completo</label>
                                    <input
                                        class="form-control"
                                        id="person_name"
                                        name="person[name]"
                                        value="{{ old('person.name') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="person_gender">Sexo</label>
                                    <select class="form-control" id="person_gender" name="person[gender]">
                                        <option value="M">Masculino</option>
                                        <option value="F" @if(old('person.gender') === 'F') selected @endif>Feminino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="person_document">CPF</label>
                                    <input
                                        class="form-control"
                                        id="person_document"
                                        name="person[document]"
                                        value="{{ old('person.document') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="person_since">Data de nascimento</label>
                                    <input
                                        type="date"
                                        class="form-control"
                                        id="person_since"
                                        name="person[since]"
                                        value="{{ old('person.since') }}">
                                </div>
                            </div>
                            <hr>
                        @endguest
                        <h2>Endereço de entrega</h2>
                        @include('addreess', ['id' => 'address', 'name' => 'address[0]', 'field' => 'address.0'])
                        <hr>
                        <h2>Pagamento</h2>
                        <div class="custom-control custom-radio">
                            <input
                                type="radio"
                                class="custom-control-input"
                                id="payment_method_credit_card"
                                name="payment_method"
                                value="credit_card"
                                @if (old('payment_method') === 'credit_card') checked @endif>
                            <label class="custom-control-label" for="payment_method_credit_card">Cartão de crédito</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input
                                type="radio"
                                class="custom-control-input"
                                id="payment_method_boleto"
                                name="payment_method"
                                value="boleto"
                                @if (old('payment_method') === 'boleto') checked @endif>
                            <label class="custom-control-label" for="payment_method_boleto">Boleto</label>
                        </div>
                        <div>
                            <div class="form-row">
                                <div class="form-group col-md-9">
                                    <label for="card_number">Número</label>
                                    <input
                                        class="form-control"
                                        id="card_number"
                                        name="card_number"
                                        value="{{ old('card_number') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="card_cvv">CVV</label>
                                    <input
                                        class="form-control"
                                        id="card_cvv"
                                        name="card_cvv"
                                        value="{{ old('card_cvv') }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-9">
                                    <label for="card_holder_name">Nome</label>
                                    <input
                                        class="form-control"
                                        id="card_holder_name"
                                        name="card_holder_name"
                                        value="{{ old('card_holder_name') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="card_expiration_date">Válido até</label>
                                    <input
                                        class="form-control"
                                        id="card_expiration_date"
                                        name="card_expiration_date"
                                        value="{{ old('card_expiration_date') }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="costumer_document">CPF</label>
                                    <input
                                        class="form-control"
                                        id="costumer_document"
                                        name="costumer_document"
                                        value="{{ old('costumer_document') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="costumer_birthday">Data de nascimento</label>
                                    <input
                                        type="date"
                                        class="form-control"
                                        id="costumer_birthday"
                                        name="costumer_birthday"
                                        value="{{ old('costumer_birthday') }}">
                                </div>
                            </div>
                        </div>
                        <h2>Endereço de cobrança</h2>
                        @include('addreess', ['id' => 'billing', 'name' => 'address[1]', 'field' => 'address.1'])
                        <button class="btn btn-primary">Fechar pedido</button>
                    </form>
                </div>
            </div>--}}


@endsection